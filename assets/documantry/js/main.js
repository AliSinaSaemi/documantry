$(function () {
  $("#search_input").data("holder", $("#search_input").attr("placeholder"));

  $("#search_input").focusin(function () {
    $(this).attr("placeholder", "Please Let Us Know Your Problem!");
  });
  $("#search_input").focusout(function () {
    $(this).attr("placeholder", $(this).data("holder"));
  });
});

$(document).ready(function () {
  // Add smooth scrolling to all links
  $(".section_list").on("click", function (event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $("html, body").animate(
        {
          scrollTop: $(hash).offset().top,
        },
        800,
        function () {
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        }
      );
    } // End if
  });
});

(function ($) {
  $(document).ready(function () {
    $("#cssmenu li.active").addClass("open").children("ul").show();
    $("#cssmenu li.has-sub>a").on("click", function () {
      $(this).removeAttr("href");
      var element = $(this).parent("li");
      if (element.hasClass("open")) {
        element.removeClass("open");
        element.find("li").removeClass("open");
        element.find("ul").slideUp(200);
      } else {
        element.addClass("open");
        element.children("ul").slideDown(200);
        element.siblings("li").children("ul").slideUp(200);
        element.siblings("li").removeClass("open");
        element.siblings("li").find("li").removeClass("open");
        element.siblings("li").find("ul").slideUp(200);
      }
    });
  });
})(jQuery);

$(document).ready(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $("#scroll").fadeIn();
    } else {
      $("#scroll").fadeOut();
    }
  });
  $("#scroll").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });
});
